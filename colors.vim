""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" COLOR
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set t_Co=256 " Terminal colors 256
" Enable syntax
syntax enable
if has('gui_running')
  set background=light
  color github
else
  set background=dark
  color monokain
endif
set synmaxcol=800
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" End COLOR
"'""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
