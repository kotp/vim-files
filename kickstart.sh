#!/bin/sh
echo "Cloning vundle to .vim folder"
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
echo "Creating symlink for your .vimrc"
ln -s ~/.vim/vimrc ~/.vimrc
echo "Creating symlink for ultisnips/ftdetect"
cd $HOME/.vim
ln -s $HOME/.vim/bundle/ultisnips/ftdetect
echo "Auto installing plugins using Vundle"
vim +PluginInstall +qall
