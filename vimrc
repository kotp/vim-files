" important
" Setings for Vim to use Vundle
" It holds the plugin list
source $HOME/.vim/plugins.vim
set nocompatible
" I want UTF almost always
set encoding=utf-8

" moving around, searching and patterns
" " " " " " " " " " " " " " "
" Useful bubble text normal mapping for arrow keys.
nnoremap <UP> ddkP
nnoremap <DOWN> ddp
vnoremap <DOWN> xp`[V`]
vnoremap <UP> xkP`[V`]

" tags
" displaying text
source $HOME/.vim/colors.vim

" syntax, highlighting and spelling

" multiple windows
" multiple tab pages
" Vim to show status regardless of number of tabs
set laststatus=3
" Set command height to 3
set ch=3

" terminal
" using the mouse
" GUI
" printing
" messages and info
" selecting text

" editing text

" tabs and indenting
" folding
" diff mode

" mapping
" " " " " " " " " " " " " " "
" Fugitive
nnoremap <leader>Gb :Gblame<cr>

" " " " " " " " " " " " " " "
" Scratch - The way to open a new buffer that won't get written
nnoremap <leader><tab> :Sscratch<cr>

" " " " " " " " " " " " " " "
" Scratch - The way to open a new buffer that won't get written
" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-b>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" neocomplete
let g:neocomplete#enable_at_startup = 1

" reading and writing files
" the swap file
" command line editing

" executing external commands
set exrc " Allow reading of .vimrc files in current folder

" running make and jumping to errors
" language specific
" multi-byte characters
" various

" Set the shell to zsh
set shell=/bin/zsh

" Want to use ctrlp plugin, but interferes with You Complete Me.
nmap <silent> <Leader>cp :CtrlP<cr>


" You Complete Me specific options

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" Get past the delimiter and open a new line
imap <C-c> <CR><Esc>O

" I like having the numbers in the gutter
set number

" Allows matchit to run
runtime macros/matchit.vim

" Allow the cursor to go in to "invalid" places
set virtualedit=all

" Backslash will update file if needed
map \ :up<CR>

" I like knowing where I am
set cursorline

" Make Vim able to edit crontab files again.
set backupskip=/tmp/*,/private/tmp/*"

" Probably don't really need backups any more.
set nobackup
set nowritebackup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp

" Get that filetype stuff happening
filetype on

" Time out on key codes but not mappings.
" Basically this makes terminal Vim work sanely.
set notimeout
set ttimeout
set ttimeoutlen=100

" Open files at last position or move to closest valid position
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif

augroup END

" ignore Rubinius, Sass cache files
set wildignore+=tmp/**,*.rbc,.rbx,*.scssc,*.sassc
" ignore Bundler standalone/vendor installs & gems
set wildignore+=bundle/**,vendor/bundle/**,vendor/cache/**,vendor/gems/**

" Keep some context around line you are on
set scrolloff=5

" Tab settings
set tabstop=2 " a tab is two spaces
set shiftwidth=2 " an autoindent (with <<) is two spaces
set expandtab " use spaces, not tabs
set backspace=indent,eol,start " Backspace through everything (but avoid backspace in general)

" Show invisible characters
set list
" Indicator characters for invisible characters
""" Note:  This may not show if you are not using a UTF-8 compatibe
"""        terminal/editor
set listchars=tab:▸\ ,trail:•,extends:❯,precedes:❮
set showbreak=\

"" Searching
set hlsearch   " highlight matches
set incsearch  " incremental searching
set ignorecase " searches are case insensitive...
set smartcase  " If you use case then use case

" Requires exuberant tags
map <Leader>rt :!ctags --extra=+f -R *<CR><CR>

" Instead of deleting what is being replaced, show a dollar sign
set cpoptions=ces$
" Turn on that syntax highlighting
syntax on

" Let's you have buffers hidden but not written
set hidden

" Don't update the display while executing macros
set lazyredraw

" Indicate current mode
set showmode

" Enable enhanced command-line completion. Presumes you have compiled
" with +wildmenu.  See :help 'wildmenu'
set wildmenu
set wildmode=list:full

" Let's make it easy to edit this file (mnemonic for the key sequence is
" 'e'dit 'v'imrc)
nmap <silent> <Leader>ev :e $MYVIMRC<cr>

" And to update and source this file as well (mnemonic for the key sequence is
" 's'ource 'v'imrc)
nmap <silent> <Leader>sv :up<cr>:so $MYVIMRC<cr>

" Airline status bar
 let g:airline_powerline_fonts=1

if v:version > 703 || v:version == 703 && has("patch541")
  set formatoptions+=j " Delete comment char when joining commented lines
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Highlighting the evil ending whitespace but not obnoxiously
" (Credit: http://sartak.org/2011/03/end-of-line-whitespace-in-vim.html)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd InsertEnter * syn clear EOLWS | syn match EOLWS excludenl /\s\+\%#\@!$/
autocmd InsertLeave * syn clear EOLWS | syn match EOLWS excludenl /\s\+$/
highlight EOLWS ctermbg=red guibg=red

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Killing the evil ending whitespace
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <leader>ews :FixWhitespace<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Loading personal settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if filereadable(expand("~/.vim.personal"))
  source ~/.vim.personal
endif

