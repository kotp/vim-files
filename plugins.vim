" Setings for Vim to use Vundle
filetype off " later we will turn it back on
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
  Plugin 'git@github.com:gmarik/Vundle.vim'
  Plugin 'git@github.com:tpope/vim-fugitive'
  Plugin 'git@github.com:MarcWeber/vim-addon-mw-utils'
  Plugin 'git@github.com:tomtom/tlib_vim'
  Plugin 'git@github.com:tkhren/vim-fake'
  Plugin 'git@github.com:tpope/vim-surround'
  Plugin 'git@github.com:tpope/vim-unimpaired'
  Plugin 'git@github.com:scrooloose/syntastic'
  Plugin 'git@github.com:vimwiki/vimwiki'
  Plugin 'git@github.com:tpope/vim-rails'
  Plugin 'git@github.com:ecomba/vim-ruby-refactoring'
" He has collected all the colorschemes
  Plugin 'git@github.com:flazz/vim-colorschemes'
  Plugin 'git@github.com:terryma/vim-multiple-cursors'
  Plugin 'git@github.com:bling/vim-airline'
  Plugin 'git@github.com:Shougo/unite.vim'
  Plugin 'git@github.com:kien/ctrlp.vim'
  Plugin 'git@github.com:mhinz/vim-signify'
  Plugin 'git@github.com:vim-ruby/vim-ruby'
  Plugin 'git@github.com:hwartig/vim-seeing-is-believing'
  Plugin 'git@github.com:Shougo/neocomplete.vim'
" Track the engine.
  Plugin 'git@github.com:SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
  Plugin 'git@github.com:honza/vim-snippets'
" For JavaScript and node.js
  Plugin 'git@github.com:pangloss/vim-javascript'
  Plugin 'git@github.com:moll/vim-node'
  Plugin 'git@github.com:marijnh/tern_for_vim'
  Plugin 'git@github.com:jelera/vim-javascript-syntax'
  Plugin 'git@github.com:Raimondi/delimitMate' " Matching delimeters
  Plugin 'git@github.com:bronson/vim-trailing-whitespace'
  Plugin 'git@github.com:ngmy/vim-rubocop'
  Plugin 'git@github.com:vim-scripts/scratch.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" Forget being compatible with good ol' vi
set nocompatible
