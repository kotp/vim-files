# Vim Configuration for Vic

This is my vim configurataion from scratch

The `kickstart.sh` script in `~/.vim` (You did clone to that folder, right?)
will set up the symlinks for .vimrc and `~/.vim/ftdetect` as necessary.

It will auto install the plugins.  See `kickstart.sh` for details.

This configuration uses the following plugins:

* ctrlp.vim
* delimitMate
* neocomplete.vim
* scratch.vim
* syntastic
* tern_for_vim
* tlib_vim
* ultisnips
* unite.vim
* vim-addon-mw-utils
* vim-airline
* vim-colorschemes
* vim-fugitive
* vim-gitgutter
* vim-javascript
* vim-javascript-syntax
* vim-matchit_ruby
* vim-multiple-cursors
* vim-node
* vim-rails
* vim-rubocop
* vim-ruby
* vim-ruby-refactoring
* vim-seeing-is-believing
* vim-signify
* vim-snippets
* vim-surround
* vim-trailing-whitespace
* vim-unimpaired
* vimwiki
